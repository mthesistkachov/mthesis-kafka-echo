module mthesis-kafka-echo

go 1.16

require (
	github.com/klauspost/compress v1.15.2 // indirect
	github.com/segmentio/kafka-go v0.4.31
)
