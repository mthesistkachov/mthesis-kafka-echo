#!/bin/bash

dlvPID=$(ps -A | grep dlv | awk '{print $1}')

if [ -n "$dlvPID" ]; then
    echo "Stopping current dlv server running at PID $dlvPID"
    printf "exit\nY\n" | dlv connect 127.1:40000 > /dev/null # stop existing session
fi

# Start the application immediately
dlv --listen=:40000 --headless=true --api-version=2 --accept-multiclient exec --continue ./main

# Wait until a debug client connected
#dlv --listen=:40000 --headless=true --api-version=2 --accept-multiclient exec ./main