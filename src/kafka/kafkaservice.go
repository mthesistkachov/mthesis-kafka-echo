package kafka

import (
	"context"
	"errors"
	"github.com/segmentio/kafka-go"
	"log"
)

type kafkaService struct {
	reader *kafka.Reader
	writer *kafka.Writer
}

func (s *kafkaService) readMessage() (string, error) {
	m, err := s.reader.ReadMessage(context.Background())
	if err != nil {
		return "", err
	}

	return string(m.Value), nil
}

func (s *kafkaService) writeMessage(message string) error {
	err := s.writer.WriteMessages(context.Background(),
		kafka.Message{
			Key:   []byte("message"),
			Value: []byte(message),
		},
	)

	if err != nil {
		switch err.(type) {
		case kafka.WriteErrors:
			err1 := err.(kafka.WriteErrors)
			msg := ""
			for _, v := range err1 {
				if v != nil {
					msg += v.Error() + "|"
				}
				return errors.New(msg)
			}
		default:
			return err
		}
	}

	return nil
}

func (s *kafkaService) Listen() {
	for {
		message, err := s.readMessage()
		if err != nil {
			log.Printf("failed wo read message: %v\n", err)
		} else {
			log.Printf("read message: %s\n", message)
		}

		message = "Echo: " + message

		err = s.writeMessage(message)
		if err != nil {
			log.Printf("failed to write message: %v\n", err)
		} else {
			log.Printf("written message: %s\n", message)
		}
	}
}

func (s *kafkaService) Close() {
	s.reader.Close()
	s.writer.Close()
}

func MakeKafkaService() *kafkaService {
	kafkaHost := "kafka:9092"

	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{kafkaHost},
		Topic:   "echo-request",
		GroupID: "mthesis",
	})

	w := &kafka.Writer{
		Addr:                   kafka.TCP(kafkaHost),
		Topic:                  "echo-response",
		ErrorLogger:            kafka.LoggerFunc(log.Printf),
		AllowAutoTopicCreation: true,
	}

	return &kafkaService{
		reader: r,
		writer: w,
	}
}
