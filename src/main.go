package main

import (
	"mthesis-kafka-echo/src/kafka"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	kafkaSvc := kafka.MakeKafkaService()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		<- sigs
		kafkaSvc.Close()
	}()

	kafkaSvc.Listen()
}
